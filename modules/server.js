import http from 'http'
import fs from 'fs'

import React from 'react'
import { renderToString } from 'react-dom/server'
import { match, RoutingContext } from 'react-router'

import DataWrapper from './react/DataWrapper'

import { createPage, write, writeError, writeNotFound, redirect } from './utils/server-utils'
import routes from '../modules/react/Routes'

var css = fs.readFileSync('public/style.css');

import { reactU } from '../react-universal/index'

const PORT = process.env.PORT || 5000

function renderApp(renderProps, res) {

   reactU.data.get({renderProps}, function(data){

      const markup = renderToString(
         <DataWrapper initProp={{ data }} >
            <RoutingContext {...renderProps}/>
         </DataWrapper>)

      const html = createPage(markup, data, css )
      write(html, 'text/html', res)

    })

}

let server = http.createServer((req, res) => {

  if (req.url === '/favicon.ico') {

    write('haha', 'text/plain', res)


  } else if (/__build__/.test(req.url)) {

    fs.readFile(`.${req.url}`, (err, data) => {
      write(data, 'text/javascript', res)
    })

  } else {

    match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
      if (error){
        writeError('ERROR!', res)
      }else if (redirectLocation){
        redirect(redirectLocation, res)
      }else if (renderProps){
        renderApp(renderProps, res)
      }else{
        writeNotFound(res)
      }
    })

  }

}).listen(PORT)

reactU.socket(server)

console.log(`listening on port ${PORT}`)







