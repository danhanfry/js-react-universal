import React from 'react';

class DataWrapper extends React.Component {

    getChildContext() {
        return {
            initProp: this.props.initProp,
        };
    }

    render() {
        return this.props.children;
    }

}

DataWrapper.childContextTypes = {
   initProp: React.PropTypes.object.isRequired,
};

export default DataWrapper;
