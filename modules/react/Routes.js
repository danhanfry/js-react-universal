import Index from './components/Index'
import Time from './components/Time'
import Saludo from './components/Saludo'


export default [
   {
      path: '/',
      component: Index,
      stateType: ['title','secon']
   },
   {
      path: 'time',
      component: Time,
      stateType: [
         'secons',
         {stateName:'seconsUp', stateMethod:'secons', update:true},
         'title'
      ]
   },
   {
      path: 'saludo',
      component: Saludo,
      stateType: [{stateName:'secons', update:false},'title']
   }
]


/*

stateName
stateMethod
filterStart
filterEnd
update : true
realtime : event


*/
