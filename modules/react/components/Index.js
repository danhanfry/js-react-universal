
import React from 'react'

import Links from './theme/Links'


export default class Index extends React.Component {

   constructor(props, context) {

      super(props, context)

      this.state = {color:'rojo'}

      console.log('[Index]', this.state )

   }

   render() {
      return (
         <div>
            <Links />
            <h2>Index {this.state.color} </h2>
         </div>
      )
   }

}

Index.contextTypes = { initProp: React.PropTypes.object.isRequired }
