import React from 'react'

import Links from './theme/Links'

import { reactU } from '../../../react-universal/client'


export default class Saludo extends React.Component {

   constructor(props, context) {

      super(props, context)

      this.state = reactU.constructor(this, {
         page: 'saludo'
      })

      console.log( this.state )

   }

   componentDidMount() {
      reactU.componentDidMount(this)
   }

   componentWillUnmount(){
      reactU.componentWillUnmount(this)
   }

   render() {
      return (
         <div>
            <Links />
            <h2>{this.state.title +' '+ this.state.page }</h2>
            <ul>
               <li><span>Segundos </span>{this.state.secons}</li>
               <li><span>Segundos </span>{this.state.secons}</li>
            </ul>
         </div>
      )
   }

}

Saludo.contextTypes = { initProp: React.PropTypes.object.isRequired }

