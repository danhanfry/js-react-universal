import React from 'react'

import Links from './theme/Links'

import { reactU } from '../../../react-universal/client'


export default class Time extends React.Component {

   constructor(props, context) {

      super(props, context)

      this.state = reactU.constructor(this, {
         page: 'time'
      })

      console.log( this.state )

   }

   componentDidMount() {
      reactU.componentDidMount(this)
   }

   componentWillUnmount(){
      reactU.componentWillUnmount(this)
   }

   render() {
      return (
         <div>
            <Links page='time' />
            <h2>{this.state.title +' '+ this.state.page }</h2>
            <ul>
               <li><span>Segundos </span>{this.state.seconsUp}</li>
               <li><span>Segundos </span>{this.state.secons}</li>
            </ul>
         </div>
      )
   }

}

Time.contextTypes = { initProp: React.PropTypes.object.isRequired }

