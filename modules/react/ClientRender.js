import React from 'react'
import { match, Router } from 'react-router'
import { render, ReactDOM} from 'react-dom'
import { createHistory } from 'history'
import routes from './Routes'

import DataWrapper from './DataWrapper'

const { pathname, search, hash } = window.location
const location = `${pathname}${search}${hash}`

let data = {}

if(window.__DATA__){
   data = window.__DATA__
}

import io from 'socket.io-client';
var socket = io('http://localhost:5000')

match({ routes, location }, () => {
   render(
      <DataWrapper initProp={{ data, socket }} >
         <Router history={ createHistory() }>{ routes }</Router>
      </DataWrapper>,
      document.getElementById('app')
   )
})
