import async from 'async'
import config from '../config'

let data = {}
export { data }


data.get = function({renderProps}, cb){

   let vParams = renderProps.params
   let vRoute = renderProps.routes[0]

   //| Comprovamos que este bien definida la ruta
   if(!renderProps.routes[0].stateType ){

      if(!vRoute.path){ vRoute.path = renderProps.location.pathname}

      let jData = {}
      jData[vRoute.path] = {}
      return cb(jData)

   }else{

      let vStateType = renderProps.routes[0].stateType
      let vStateUp = data.getStateUp(vStateType)

      data.getState({vRoute, vParams}, function( vState ){

         let jData = {}
         jData[vRoute.path] = { vStateType, vStateUp, vState}

         return cb(jData)

      })

   }


}

data.getUp = function({renderProps}, cb){

   let vParams = renderProps.params
   let vRoute = renderProps.routes[0]

   //| Comprovamos que este bien definida la ruta
   if(!renderProps.routes[0].stateType ){

      if(!vRoute.path){ vRoute.path = renderProps.location.pathname}

      let jData = {}
      jData[vRoute.path] = {}
      return cb(jData)

   }else{

      let vStateType = renderProps.routes[0].stateType
      let vStateUp = data.getStateUp(vStateType)

      data.getState({vRoute, vParams, sDataSocket:true}, cb)

   }


}






//| Devuelve todos los valors de cada state
data.getState = function({vRoute, vParams, sDataSocket}, cb) {

   let jState = {}

   //| Recorremos cada State de la ruta
   async.map(vRoute.stateType, function(i_stateType, each_cb_stateType){

      if(!sDataSocket || sDataSocket == false){

         mapMethod({i_stateType, each_cb_stateType})

      }else if(sDataSocket == true && i_stateType.update == true){

         mapMethod({i_stateType, each_cb_stateType})

      }else{
         return each_cb_stateType()
      }


   }, function(){

      return cb(jState)

   })

   function mapMethod({i_stateType, each_cb_stateType}){

      //| Si StateType es un string le añadimos los keys q faltan
      if( isType(i_stateType, 'string') ){
         i_stateType = {
            stateName: i_stateType,
            stateMethod: i_stateType
         }
      }
      if(!i_stateType.stateName ){
         i_stateType.stateName = i_stateType.stateMethod
      }
      if(!i_stateType.stateMethod ){
         i_stateType.stateMethod = i_stateType.stateName
      }

      //| Primer metodo de filtrado antes de traer los datos
      if(!i_stateType.filterStart){ return _state() }

      let vfilterStart = null
      if(config.filter[i_stateType.filterStart]){

         vfilterStart = config.filter[i_stateType.filterStart]({vRoute}, _state)

         if(vfilterStart){ return _state(vfilterStart) }

      }else{

         return _state(vfilterStart)

      }

      //| Capturamos el state llamando al method
      function _state(vfilterStart){

         if(config.state[i_stateType.stateMethod]){

            let vState = config.state[i_stateType.stateMethod]({vParams, vfilterStart}, _filterEnd)

            if(vState){ return _filterEnd(vState) }

         }else{

            return _filterEnd(null)

         }

      }

      //| Ultimo filtro antes de devolver el estado
      function _filterEnd(vState){

         if(!i_stateType.filterEnd){ return _setDataJson(vState) }

         if(config.filter[i_stateType.filterEnd]){
            let vStateFilter = config.filter[i_stateType.filterEnd](
                                 {vState, vParams, vfilterStart}, _setDataJson)

            if(vStateFilter){ return _setDataJson(vStateFilter) }

         }else{

            return _setDataJson(vState)

         }

      }

      function _setDataJson(vStateFilter){

         jState[i_stateType.stateName] = vStateFilter

         return each_cb_stateType()

      }

   }

}



//| Devuelve los state q requieran update de la ruta actual
data.getStateUp = function(vStateType) {

   let vStateUp = []

   if(vStateUp){
      vStateUp = vStateType.filter(function(json) {
         return json.update === true
      })
   }

   return vStateUp

}




 function isType(object, type) {

	if (object === null) {
	    return null;
	}
	else if (object === undefined) {
	    return undefined;
	}

	else if (object.constructor === Number(1).constructor) {

		if(!type){ return "number" }
		if(type == 'number'){
			return true
		}else{
			return false
		}

	}
	else if (object.constructor === "test".constructor) {

	    if(!type){ return "string" }
		if(type == 'string'){
			return true
		}else{
			return false
		}
	}
	else if (object.constructor === [].constructor) {

	    if(!type){ return "array" }
		if(type == 'array'){
			return true
		}else{
			return false
		}
	}
	else if (object.constructor === {}.constructor) {

	    if(!type){ return "json" }
		if(type == 'json'){
			return true
		}else{
			return false
		}
	}
	else {
	    return null;
	}

}




