
import { match } from 'react-router'
import io from 'socket.io'
import config from '../config'
import { data } from './data'

export function setSocket(server){

   let routes = config.routes

   let vSocket = io.listen(server);

   vSocket.on('connection', function(socket) {

      socket.on('init', function(vPath){

         console.log('init', vPath)

         match({ routes, location: vPath }, (error, redirectLocation, renderProps) => {

             data.get({renderProps}, function(data){

                socket.emit('init', data)

             })

         })

      })

      socket.on('init:update', function(vPath){

         console.log('init:update', vPath);

         match({ routes, location: vPath }, (error, redirectLocation, renderProps) => {

             data.getUp({renderProps}, function(vState){

                socket.emit('init:update', vState)

             })

         })

      })

   })

}

//let socket = io.listen(server);

//module.exports = function(io){

//   console.log('hola')

//   io.on('connection', function(socket) {
//
//      socket.on('init', function(vPath){
//
////         console.log('init', vPath)
////
////         match({ routes, location: vPath }, (error, redirectLocation, renderProps) => {
////
////             sData({renderProps}, function(data){
////
////                socket.emit('init', data)
////
////             })
////
////         })
//
//      })
//
//      socket.on('init:update', function(vPath){
//
////         console.log('init:update', vPath);
////
////         match({ routes, location: vPath }, (error, redirectLocation, renderProps) => {
////
////             sDataSocket({renderProps}, function(vState){
////
////                socket.emit('init:update', vState)
////
////             })
////
////         })
//
//
//      })
//
//   })

//}
