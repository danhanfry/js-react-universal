


let reactU = {}
export { reactU }


import config from './config'
reactU.config = config


import { data } from './server/data'
reactU.data = data


import { setSocket } from './server/socket'
reactU.socket = setSocket
