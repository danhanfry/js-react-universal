
let reactU = {}
export { reactU }

reactU.constructor = function(_this, state) {

   _this.socket = _this.context.initProp.socket
   let vPath = _this.props.route.path || _this.props.location.pathname
   let vData = _this.context.initProp.data[vPath] || {}
   let vState = vData.vState || {_server: false}

   var assign = Object.assign(state, vState);

   return assign || {}

}

reactU.componentDidMount = function(_this) {

   let vPath = _this.props.route.path
   let vPathParams = _this.props.location.pathname
   let vParams = _this.props.params
   let vDataPaht = _this.context.initProp.data[vPath] || {}
   let vParamsMemory = vDataPaht.paramsMemory || {}

   if(_this.state._server == false || isEquivalent(vParams, vParamsMemory) == false ){
      _this.socket.emit('init', vPathParams)
      _this.socket.on('init', socket_init)
   }

   function socket_init(data){
      _this.setState(data[vPath].vState)
      _this.context.initProp.data[vPath] = data[vPath]
      _this.context.initProp.data[vPath].paramsMemory = vParams
   }

   if(vDataPaht.vStateUp && vDataPaht.vStateUp[0] ){

      _this.socket.emit('init:update', vPathParams)
      _this.socket.on('init:update', function(vState){
         _this.setState(vState)

      })

   }

}

reactU.componentWillUnmount = function(_this) {
   _this.socket.removeListener('init');
   _this.socket.removeListener('init:update');
}

function isEquivalent(a, b) {
    var aProps = Object.getOwnPropertyNames(a);
    var bProps = Object.getOwnPropertyNames(b);
    if (aProps.length != bProps.length) {
        return false;
    }
    for (var i = 0; i < aProps.length; i++) {
        var propName = aProps[i];
        if (a[propName] !== b[propName]) {
            return false;
        }
    }
    return true;
}




