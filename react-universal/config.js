
import routes from '../modules/react/Routes'
import { state } from '../modules/services/state.js'
import { filter } from '../modules/services/filter.js'

export default {routes, state, filter}
